<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\CategoryController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Product Api Routes

Route::group([
    'middleware' => 'api',
    'prefix' => 'product'
], function ($router) {
    Route::get('get', [ProductController::class, 'get']);    
    Route::get('total', [ProductController::class,'total']);
    Route::get('out_of_stock',[ProductController::class,'outOfStock']);
    Route::post('store', [ProductController::class,'store']);
    Route::delete('delete/{id}', [ProductController::class,'delete']);
    Route::put('update', [ProductController::class,'update']);
});

//Category Api Routes
Route::group([
    'middleware' => 'api',
    'prefix' => 'category'
], function ($router) {

    Route::get('get', [CategoryController::class, 'get']);    
    Route::get('total', [CategoryController::class, 'total']);
    Route::post('store',[CategoryController::class, 'store']);
    Route::delete('delete/{id}', [CategoryController::class, 'delete']);
    Route::put('update', [CategoryController::class, 'update']);
});

//Auth Api Routes
Route::group([

    'middleware' => 'api',
    'prefix' => 'auth'

], function ($router) {
    
    Route::post('register', [AuthController::class, 'register']);
    Route::post('verifyToken', [AuthController::class, 'verifyToken']);
    Route::post('login',[AuthController::class, 'login']);
    Route::post('logout', [AuthController::class, 'logout']);
    Route::post('refresh', [AuthController::class, 'refresh']);
    Route::get('user', [AuthController::class, 'user']);

});
